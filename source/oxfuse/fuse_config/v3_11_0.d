/*
 * This file distributed under the terms of the GNU LGPLv2.
 * The module based on data from https://github.com/libfuse/libfuse
 */

/// The module contains `fuse_config` from libfuse ≥ 3.11 but < 3.14.1.
module oxfuse.fuse_config.v3_11_0;


/**
 * Configuration of the high-level API.
 */
struct fuse_config {
    /**
     * If `set_gid` is non-zero, the st_gid attribute of each file
     * is overwritten with the value of `gid`.
     */
    int set_gid;
    uint gid;

    /**
     * If `set_uid` is non-zero, the st_uid attribute of each file
     * is overwritten with the value of `uid`.
     */
    int set_uid;
    uint uid;

    /**
     * If `set_mode` is non-zero, the any permissions bits set in
     * `umask` are unset in the st_mode attribute of each file.
     */
    int set_mode;
    uint umask;

    /**
     * The timeout in seconds for which name lookups will be
     * cached.
     */
    double entry_timeout;

    /**
     * The timeout in seconds for which a negative lookup will be
     * cached. This means, that if file did not exist (lookup
     * returned ENOENT), the lookup will only be redone after the
     * timeout, and the file/directory will be assumed to not
     * exist until then. A value of zero means that negative
     * lookups are not cached.
     */
    double negative_timeout;

    /**
     * The timeout in seconds for which file/directory attributes
     * (as returned by e.g. the `getattr` handler) are cached.
     */
    double attr_timeout;

    /**
     * Allow requests to be interrupted
     */
    int intr;

    /**
     * Specify which signal number to send to the filesystem when
     * a request is interrupted.  The default is hardcoded to
     * USR1.
     */
    int intr_signal;

    /**
     * Normally, FUSE assigns inodes to paths only for as long as
     * the kernel is aware of them. With this option inodes are
     * instead remembered for at least this many seconds.  This
     * will require more memory, but may be necessary when using
     * applications that make use of inode numbers.
     *
     * A number of -1 means that inodes will be remembered for the
     * entire life-time of the file-system process.
     */
    int remember;

    /**
     * The default behavior is that if an open file is deleted,
     * the file is renamed to a hidden file (.fuse_hiddenXXX), and
     * only removed when the file is finally released.  This
     * relieves the filesystem implementation of having to deal
     * with this problem. This option disables the hiding
     * behavior, and files are removed immediately in an unlink
     * operation (or in a rename operation which overwrites an
     * existing file).
     *
     * It is recommended that you not use the hard_remove
     * option. When hard_remove is set, the following libc
     * functions fail on unlinked files (returning errno of
     * ENOENT): read(2), write(2), fsync(2), close(2), f*xattr(2),
     * ftruncate(2), fstat(2), fchmod(2), fchown(2)
     */
    int hard_remove;

    /**
     * Honor the st_ino field in the functions getattr() and
     * fill_dir(). This value is used to fill in the st_ino field
     * in the stat(2), lstat(2), fstat(2) functions and the d_ino
     * field in the readdir(2) function. The filesystem does not
     * have to guarantee uniqueness, however some applications
     * rely on this value being unique for the whole filesystem.
     *
     * Note that this does *not* affect the inode that libfuse
     * and the kernel use internally (also called the "nodeid").
     */
    int use_ino;

    /**
     * If use_ino option is not given, still try to fill in the
     * d_ino field in readdir(2). If the name was previously
     * looked up, and is still in the cache, the inode number
     * found there will be used.  Otherwise it will be set to -1.
     * If use_ino option is given, this option is ignored.
     */
    int readdir_ino;

    /**
     * This option disables the use of page cache (file content cache)
     * in the kernel for this filesystem. This has several affects:
     *
     * 1. Each read(2) or write(2) system call will initiate one
     *    or more read or write operations, data will not be
     *    cached in the kernel.
     *
     * 2. The return value of the read() and write() system calls
     *    will correspond to the return values of the read and
     *    write operations. This is useful for example if the
     *    file size is not known in advance (before reading it).
     *
     * Internally, enabling this option causes fuse to set the
     * `direct_io` field of `struct fuse_file_info` - overwriting
     * any value that was put there by the file system.
     */
    int direct_io;

    /**
     * This option disables flushing the cache of the file
     * contents on every open(2).  This should only be enabled on
     * filesystems where the file data is never changed
     * externally (not through the mounted FUSE filesystem).  Thus
     * it is not suitable for network filesystems and other
     * intermediate filesystems.
     *
     * NOTE: if this option is not specified (and neither
     * direct_io) data is still cached after the open(2), so a
     * read(2) system call will not always initiate a read
     * operation.
     *
     * Internally, enabling this option causes fuse to set the
     * `keep_cache` field of `struct fuse_file_info` - overwriting
     * any value that was put there by the file system.
     */
    int kernel_cache;

    /**
     * This option is an alternative to `kernel_cache`. Instead of
     * unconditionally keeping cached data, the cached data is
     * invalidated on open(2) if if the modification time or the
     * size of the file has changed since it was last opened.
     */
    int auto_cache;

    /**
     * By default, fuse waits for all pending writes to complete
     * and calls the FLUSH operation on close(2) of every fuse fd.
     * With this option, wait and FLUSH are not done for read-only
     * fuse fd, similar to the behavior of NFS/SMB clients.
     */
    int no_rofd_flush;

    /**
     * The timeout in seconds for which file attributes are cached
     * for the purpose of checking if auto_cache should flush the
     * file data on open.
     */
    int ac_attr_timeout_set;
    double ac_attr_timeout;

    /**
     * If this option is given the file-system handlers for the
     * following operations will not receive path information:
     * read, write, flush, release, fallocate, fsync, readdir,
     * releasedir, fsyncdir, lock, ioctl and poll.
     *
     * For the truncate, getattr, chmod, chown and utimens
     * operations the path will be provided only if the struct
     * fuse_file_info argument is null.
     */
    int nullpath_ok;

    /**
     * The remaining options are used by libfuse internally and
     * should not be touched.
     */
    int show_help;
    char* modules;
    int debug_;
}
