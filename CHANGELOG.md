# Changelog

---

## [0.4.2] - 2025-03-04

- Fix import of fuse_config module for libfuse < 3.11.0.

---

## [0.4.1] - 2025-03-04

- Fix installation system.

---

## [0.4.0] - 2025-03-04

- To prevent issues on some **libfuse** versions, max_idle_threads
  has been changed to 100'000.

- There are now several versions of the `fuse_config` structure defined
  in the project because the original **libfuse3** library broke backward
  compatibility several times. The correct version of the structure is
  determined at compile time. The same problem is solved for
  the `fuse_file_info` structure in exactly the same way.
  As a result, this means that each build of our library
  is tightly bound to a specific version of **libfuse3**.

- New wrapper classes: `ConnInfo`, `FuseConfig`, `FileInfo`.
  They will be used in file operations instead of
  `fuse_conn_info`, `fuse_config` and `fuse_file_info`.

- The signatures of the `read()` and `write()` operations have been
  modified. They now accept dynamic arrays as input/output buffers.

*Notice.*  
*Due to meaningful changes this release breaks backward compatibility.
This is not considered a bad thing for a zero major version, but we
will try to avoid such significant changes in future releases.*

---

## [0.3.4] - 2025-02-19

- Minor fixes in the build system.

---

## [0.3.3] - 2025-02-18

- Fixed 'Ignoring invalid max threads value 4294967295 > max (100000)' warning.
- Various small fixes and improvements.

---

## [0.3.2] - 2025-01-27

- Fixed behavior of `getxattr()` and `listxattr()` when buffer size is 0.

---

## [0.3.1] - 2024-12-24

- Each new C thread joins the D runtime; the GC has been enabled.

---

## [0.3.0] - 2024-11-17

- Fixed a critical bug: thread control was implemented incorrectly,
  which caused the GC could behave improperly. The GC has been disabled.

- New inferface of `mount()`: added argument for mount point path;
  methods `setMountPoint()` and `getMountPoint()` are deleted as deprecated.

---

## [0.2.1] - 2024-11-11

- Fix README.md.

---

## [0.2.0] - 2024-11-11

- The logic of the `call_destroy()` function was overly fancy.
- The `oxfuse.mount()` interface no longer requires mandatory passing
  of a file system type as a template parameter.
- Other minor changes.

---

## [0.1.2] - 2023-10-28

### Fixed

- Linking with libfuse3.
- Style and some text errors.

---

## [0.1.1] - 2023-02-19

### Fixed

- Installation path to pkg-config directory.
- Some documentation comments.

### Added

- Links to the library documentation and download page.

---

## [0.1.0] - 2023-02-17

### Added

- Initial release of a D language binding
  to [the FUSE library](https://github.com/libfuse/libfuse) v3+.
