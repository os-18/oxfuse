#!/bin/bash

# Copying and distribution of this file, with or without modification, are
# permitted in any medium without royalty provided the copyright notice and
# this notice are preserved. This file is offered as-is, without any warranty.

LIBFUSE_VERSION=$(pkg-config --modversion fuse3)
if [[ $? -ne 0 ]]; then
    echo "Error: libfuse3 and pkg-config must be installed." >&2
    exit 1
fi
V_FILE="source/oxfuse/libfuse_version.d"
echo "// This file is generated at compile time." > "$V_FILE"
echo "module oxfuse.libfuse_version;" >> "$V_FILE"
echo "enum libfuseVersion = \"$LIBFUSE_VERSION\";" >> "$V_FILE"
echo "$V_FILE has been created."
