This project is distributed in the hope that it will be useful,
but without any warranty; without even the implied warranty of
merchantability or fitness for a particular purpose.

**Official repositories**:

- [https://codeberg.org/os-18/oxfuse](https://codeberg.org/os-18/oxfuse)
- [https://gitlab.com/os-18/oxfuse](https://gitlab.com/os-18/oxfuse)


*Keywords*:  
dlang, d, fuse, libfuse, libfuse3, binding, wrapper.

---

## About

OXFuse (Object-oriented eXtension of FUSE) is a D language binding
for [libfuse3](https://github.com/libfuse/libfuse). This project
allows to write filesystem for Linux.

### Features

- To create your own file system, you should inherit from
  the `oxfuse.FileSystem` class and overload any methods (file operations).

- This library allows to use fourty operations from libfuse
  (all except `write_buf()`/`read_buf()`).

- The behavior of all non-overloaded functions is determined by FUSE
  and the OS kernel.

- The library doesn\'t contain bindings to functions which parse command line
  arguments. Parsing is proposed to be done using the D language tools.

- Error situations with accompanying messages and `errno` are based
  on `FuseException` and the `fe()` function.

- The [original library](https://github.com/libfuse/libfuse) broke backward
  compatibility for the `fuse_config` and `fuse_file_info` structures.
  Our library takes into account the current system version of libfuse3.
  All structures in the final build have the correct versions.

- Currently, only the high-level part has been ported, the low-level part
  is not available.

---

## Documentation

[https://os-18.codeberg.page/oxfuse/](https://os-18.codeberg.page/oxfuse/)

The "examples/" directory contains several different implementations
of file systems.

Useful imformation is also available here:
[https://github.com/libfuse/libfuse/wiki](https://github.com/libfuse/libfuse/wiki).

---

## Ready-made packages

See:  
[Download Page of OS-18](https://codeberg.org/os-18/os-18-docs/src/branch/master/OS-18_Packages.md)

The page contains information about installing several packages, including
OXFuse.

---

## Build from source

### Preparing

You must have [make](https://www.gnu.org/software/make/)
and at least one of these compilers:

- [Digital Mars D Compiler](https://dlang.org/download.html#dmd)
- [LLVM D Compiler](https://wiki.dlang.org/LDC)
- [GNU D Compiler](https://www.gdcproject.org)

Also the library needs **libfuse3**.

### Simple building and installing

Creating of static and dynamic libraries for the **ldc2** compiler (by default):

```bash
make
```

Installation (root rights may be required):

```bash
make install
```

The Makefile contains variables:

* `DESTDIR` specifies the root directory for installing (empty, by default);
* `PREFIX` points to the base directory like `usr/local` or `usr`.

Installation directory is defined as `$(DESTDIR)/$(PREFIX)` in the Makefile.

You can install the library files in any directory, like `$HOME/.local`:

```bash
make install DESTDIR=$HOME PREFIX=.local
```

Uninstall:

```bash
make uninstall
```

If you installed the project in some alternative directory:

```bash
make uninstall DESTDIR=$HOME PREFIX=.local
```

### Other compilers

Compiler by default is **ldc2**.
You can choose other compiler (for example, **gdc**):

```bash
make DC=gdc
```

Installing:

```bash
make install DC=gdc
```

Uninstalling:

```bash
make uninstall DC=gdc
```

---


## Feedback

Questions, suggestions, comments, bugs:

**tech.vindex@gmail.com**

Also use the repository service tools.

---
